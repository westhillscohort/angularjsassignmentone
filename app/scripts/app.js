(function() {

    'use strict';

    angular
  		.module('angularjsTestApp', [
			'ngAnimate',
			'ngCookies',
			'ngResource',
			'ngRoute',
			'ngSanitize',
			'ngTouch',
			'angularjsTestApp.AppServicesModule'
  		])
  		.config(function ($routeProvider) {
			$routeProvider
				.when('/', {
					templateUrl: 'views/main.html',
					controller: 'Main',
					controllerAs: 'vm'
				})
				.otherwise({
					redirectTo: '/'
				});
		});
})();
