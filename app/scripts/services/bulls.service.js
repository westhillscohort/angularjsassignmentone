(function() {
    'use strict';

    angular
        .module('angularjsTestApp.AppServicesModule')
        .factory('BullsService', BullsService);

        BullsService.$inject = ['$http'];

        function BullsService($http) {
            return {
                GetBulls: GetBulls
            };

            function GetBulls() {
                return $http.get('./scripts/json/bulls.json')
                            .then(GetBullsCompleted)
                            .catch(GetBullsFailed);

                            function GetBullsCompleted(res) {
                                return res.data;
                            }

                            function GetBullsFailed(error) {
                                return error;
                            }
            }
        }
})();