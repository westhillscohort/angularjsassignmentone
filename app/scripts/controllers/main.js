(function() {
	
	'use strict';

	angular
		.module('angularjsTestApp')
		.controller('Main', Main); 
		
		Main.$inject = ['BullsService'];
		
		function Main(BullsService) {

			var vm = this;

			init();

			function init() {
				return GetDrivers(); 
			}

			function GetDrivers() {
				return BullsService.GetBulls().then(function(res) {
					vm.bulls = res.results;
					return vm.bulls;
				});
			}
		}

})();
